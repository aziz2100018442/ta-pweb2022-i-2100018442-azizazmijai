<?php
    echo "<hr> ARRAY <br><br>";
    $kode=array("alpha","beta","charlie");
    echo "Dengan For : <br>";
    for ($i=0; $i < count($kode) ; $i++) { 
        echo "Hasilnya : ". $kode[$i]."<br>";
    }
    $uwah=array("falcon"=>90,"delta"=>69,"eagle"=>55);
    echo "<br> Dengan Foreach <br>";
    foreach ($uwah as $huwek=>$nom){
        echo "Nilai : $huwek = $nom <br>";
    }
    /*while(list($huwek,$nom)=each($uwah)) {
        echo "Hasil : $huwek = $nom <br>";          ERROR pake While list 
    } */
    echo "<hr>";
    echo "Output Struktur Array <br>";
    echo "<pre>";
    print_r($kode);
    echo "</pre>";
    echo "<hr>";
    echo "Mengurutkan Array (SORT) <br>";
    sort($uwah); 
    reset($uwah);
    echo "<pre>";
    print_r($uwah);
    echo "</pre>";
    echo "<hr> Mencari Elemen Array <br>";
    if (in_array("alpha",$kode)){
        echo "Lapor alpha target telah ditangkap !!";
    }else {
        echo "Target kabur alpha !!";
    }
?>